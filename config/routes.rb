Educare::Application.routes.draw do

  root :to =>'posts#index'

  devise_for :users, :controllers => {:sessions => 'sessions', :registrations => 'registrations'}

  namespace :admin do
    resources :users
    resources :posts
    resources :categories
    resources :courses
    resources :teachers
  end

  resources :posts, :only => [:show, :index]
  resources :courses, :only => [:show, :index]
  resources :teachers, :only => [:show, :index]

  match '/gioi-thieu' => 'pages#about_us'

end
