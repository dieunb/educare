class CoursesController < ApplicationController

  def index
    @courses = Course.all.page(params[:page]).per(4)
  end

  def show
    @course = Course.find_by_slug(params[:id])
  end
end