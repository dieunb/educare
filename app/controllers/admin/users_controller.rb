class Admin::UsersController < Admin::BaseController

  def index
    @search = User.scoped_search(params[:search])
    if params[:commit] == 'Search' && !params[:from].blank? && !params[:to].blank?
      @users = User.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'), :created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    elsif params[:commit] == 'Search' && !params[:from].blank? && params[:to].blank?
      @users = User.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'))
      params[:to] = Date.today.strftime('%d/%m/%Y')
    elsif params[:commit] == 'Search' && params[:from].blank? && !params[:to].blank?
      @users = User.where(:created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    else
      @users = @search.all
      if params['sort_by_email']
        session['user_sort'] = 'sort_by_email'
        @users = @users.sort { |o1, o2| o1.email <=> o2.email }
      elsif params['sort_by_name']
        session['user_sort'] = 'sort_by_name'
        @users = @users.sort { |o1, o2| o1.last_name <=> o2.last_name }
      elsif params['sort_by_country']
        session['user_sort'] = 'sort_by_country'
        @users = @users.sort { |o1, o2| o1.country.to_s <=> o2.country.to_s }
      elsif params['sort_by_registration_date']
        session['user_sort'] = 'sort_by_registration_date'
        @users = @users.sort { |o1, o2| o1.created_at <=> o2.created_at }
      else
        session['user_sort'] = nil
      end
    end

    if params['per_page']
      per_page = params['per_page']
      session['per_page'] = params['per_page']
    else
      per_page = 20
      session['per_page'] = nil
    end

    if session['user_sort']
      @users = Kaminari.paginate_array(@users).page(params[:page]).per(per_page.to_i)
    else
      @users = @users.page(params[:page]).per(per_page.to_i)
    end
  end

  def resource
    @user ||= end_of_association_chain.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_without_password(params[:user])
      redirect_to admin_user_path(@user), notice: 'User was updated successfully'
    else
      reader 'edit'
    end
  end
end
