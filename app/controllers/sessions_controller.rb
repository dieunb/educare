class SessionsController < Devise::SessionsController
  def new
    sign_in_url = url_for(:action => "new", :controller => 'sessions', :only_path => false, :protocol => 'http')
    render 'devise/sessions/new'
  end
end