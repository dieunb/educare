class Admin::PostsController < Admin::BaseController
  def index
    @search = Post.scoped_search(params[:search])
    if params[:commit] == 'Search' && !params[:from].blank? && !params[:to].blank?
      @posts = Post.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'), :created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    elsif params[:commit] == 'Search' && !params[:from].blank? && params[:to].blank?
      @posts = Post.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'))
      params[:to] = Date.today.strftime('%d/%m/%Y')
    elsif params[:commit] == 'Search' && params[:from].blank? && !params[:to].blank?
      @posts = Post.where(:created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    else
      @posts = @search.all
      if params['sort_by_title']
        session['post_sort'] = 'sort_by_title'
        @posts = @posts.sort { |o1, o2| o1.title <=> o2.title }
      else
        session['post_sort'] = nil
      end
    end

    if params['per_page']
      per_page = params['per_page']
      session['per_page'] = params['per_page']
    else
      per_page = 20
      session['per_page'] = nil
    end

    if session['user_sort']
      @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(per_page.to_i)
    else
      @posts = @posts.page(params[:page]).per(per_page.to_i)
    end
  end

  def resource
    @post ||= end_of_association_chain.find_by_slug(params[:id])
  end

  def create
    @post = Post.new(params[:post])
    @post.user = current_user
    @post.photo = params[:post][:photo]

    if @post.save
      redirect_to admin_post_path(@post)
    else
      render 'new'
    end
  end
end