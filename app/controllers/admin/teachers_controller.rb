class Admin::TeachersController < Admin::BaseController

  def index
    @search = Teacher.scoped_search(params[:search])
    if params[:commit] == 'Search' && !params[:from].blank? && !params[:to].blank?
      @teachers = Teacher.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'), :created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    elsif params[:commit] == 'Search' && !params[:from].blank? && params[:to].blank?
      @teachers = Teacher.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'))
      params[:to] = Date.today.strftime('%d/%m/%Y')
    elsif params[:commit] == 'Search' && params[:from].blank? && !params[:to].blank?
      @teachers = Teacher.where(:created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    else
      @teachers = @search.all
      if params['sort_by_email']
        session['teacher_sort'] = 'sort_by_email'
        @teachers = @teachers.sort { |o1, o2| o1.email <=> o2.email }
      elsif params['sort_by_name']
        session['teacher_sort'] = 'sort_by_name'
        @teachers = @teachers.sort { |o1, o2| o1.full_name <=> o2.full_name }
      else
        session['user_sort'] = nil
      end
    end

    if params['per_page']
      per_page = params['per_page']
      session['per_page'] = params['per_page']
    else
      per_page = 20
      session['per_page'] = nil
    end

    if session['teacher_sort']
      @teachers = Kaminari.paginate_array(@teachers).page(params[:page]).per(per_page.to_i)
    else
      @teachers = @teachers.page(params[:page]).per(per_page.to_i)
    end
  end

  def resource
    @teacher ||= end_of_association_chain.find(params[:id])
  end
end
