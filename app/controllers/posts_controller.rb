class PostsController < ApplicationController

  def index
    @posts = Post.all.page(params[:page]).per(4)
  end

  def show
    @post = Post.find_by_slug(params[:id])
  end
end