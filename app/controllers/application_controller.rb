class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :query_headers

  protected
  rescue_from Exception, :with => :server_error
  def server_error(exception)
    if exception.is_a? CanCan::AccessDenied
      redirect_to root_url, :alert => exception.message
    else
      ExceptionNotifier::Notifier.exception_notification(request.env, exception).deliver
    end
  end

  def after_sign_in_path_for(resource_or_scope)
    path = root_path
    path = session[:after_sign_in_path] || path
    session.delete :after_sign_in_path
    path = current_user.admin? ? admin_users_path : path
    return path
  end

  def query_headers

  end
end
