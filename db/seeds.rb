puts "Creating admin ..."
Fabricate(:admin, :email => Settings.admin_email)

puts "Create 50 users"
50.times do
  Fabricate(:user, avatar: File.open("#{Rails.root}/app/assets/images/default_avatar.jpg"))
end

puts "Create 10 category"
10.times do
  Fabricate(:category)
end

puts "Create 10 teachers"
10.times do
  Fabricate(:teacher, avatar: File.open("#{Rails.root}/app/assets/images/default_avatar.jpg"))
end

puts "Create 10 course"
10.times do
  course = Fabricate(:course, teacher: Teacher.all.sample)
  course.users << User.all.sample
  course.save!
end

puts "Create 50 post"
50.times do
  Fabricate(:post,
            user: User.all.sample,
            category: Category.all.sample,
            photo: File.open("#{Rails.root}/app/assets/images/posts_images/#{rand(1..3)}.jpg"))
end