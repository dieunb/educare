class Category
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Slug
  include ScopedSearch::Model

  field :title, type: String
  field :description, type: String

  attr_accessible :title, :description, :posts

  slug :title
  has_many :posts
  # has_and_belongs_to_many :cate_ofs, class_name: "Category", inverse_of: :cates
end