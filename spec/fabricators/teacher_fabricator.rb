Fabricator(:teacher) do
  email { Faker::Internet.email }
  full_name { Faker::Name.last_name + " " + Faker::Name.first_name }
  phone_number { Faker::PhoneNumber.phone_number }
  bio { Faker::Lorem.sentence(50) }
  address { Faker::Address.street_name + " " + Faker::Address.country }
end