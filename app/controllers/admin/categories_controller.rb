class Admin::CategoriesController < Admin::BaseController

  def index
    @search = Category.scoped_search(params[:search])
    if params[:commit] == 'Search' && !params[:from].blank? && !params[:to].blank?
      @categories = Category.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'), :created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    elsif params[:commit] == 'Search' && !params[:from].blank? && params[:to].blank?
      @categories = Category.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'))
      params[:to] = Date.today.strftime('%d/%m/%Y')
    elsif params[:commit] == 'Search' && params[:from].blank? && !params[:to].blank?
      @categories = Category.where(:created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    else
      @categories = @search.all
      if params['sort_by_title']
        session['user_sort'] = 'sort_by_title'
        @categories = @categories.sort { |o1, o2| o1.title <=> o2.title }
      else
        session['user_sort'] = nil
      end
    end

    if params['per_page']
      per_page = params['per_page']
      session['per_page'] = params['per_page']
    else
      per_page = 20
      session['per_page'] = nil
    end

    if session['user_sort']
      @categories = Kaminari.paginate_array(@categories).page(params[:page]).per(per_page.to_i)
    else
      @categories = @categories.page(params[:page]).per(per_page.to_i)
    end
  end

  def resource
    @category ||= end_of_association_chain.find(params[:id])
  end
end
