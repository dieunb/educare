Fabricator(:course) do
  title { Faker::Lorem.sentence }
  description { Faker::Lorem.sentence(50) }
  body { Faker::Lorem.sentence(150) }
end