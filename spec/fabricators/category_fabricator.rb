Fabricator(:category) do
  title { Faker::Lorem.sentence }
  description { Faker::Lorem.sentence(50) }
end