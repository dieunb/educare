Fabricator(:user) do
  email { Faker::Internet.email }
  password 'password'
  password_confirmation 'password'
  full_name { Faker::Name.last_name + " " + Faker::Name.first_name }
  phone_number { Faker::PhoneNumber.phone_number }
  role { Settings.user.roles.customer }
  address { Faker::Address.street_name + " " + Faker::Address.country }
end

Fabricator(:admin, :from => :user) do
  email { Settings.admin_email }
  role { Settings.user.roles.admin }
end