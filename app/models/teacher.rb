class Teacher
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ScopedSearch::Model

  field :full_name, type: String
  field :email, type: String
  field :phone_number, type: String
  field :bio, type: String
  field :address, type: String

  field :avatar_uid, type: String
  image_accessor :avatar

  has_many :courses

  attr_accessible :full_name, :email, :phone_number, :avatar, :avatar_uid

end