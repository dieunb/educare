class RegistrationsController < Devise::RegistrationsController

  def create
    @user = User.create(params[:user])
    if @user.save
      sign_in @user, by_pass: true
      redirect_to root_path, notice: "Successful registered!"
    else
      render :new, notice: @user.errors.full_messages
    end
  end

end
