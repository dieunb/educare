source 'http://rubygems.org'

gem 'rails'

# Database
gem 'mongoid'
gem 'bson_ext'
gem 'mongoid_slug'

# Authentication & authorization
gem 'devise'
gem 'cancan'
# gem 'omniauth'

# Upload image
gem 'rack-cache', :require => 'rack/cache'
gem 'dragonfly'

gem 'simple_form'
gem 'country_select'
gem 'web-app-theme', :git =>'git://github.com/pilu/web-app-theme.git'
gem 'settingslogic'
gem 'inherited_resources'
gem 'scoped-search', :require => 'scoped_search'

# Pagination
gem 'kaminari'

# Exception notifier
gem "exception_notification", :git => "git://github.com/rails/exception_notification", :require => 'exception_notifier'

# Generate sample data
gem 'fabrication'
gem 'faker'

# ckeditor
gem 'ckeditor_rails'

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'bootstrap-sass'
  gem 'jquery-rails'
  gem 'uglifier', '>= 1.0.3'
  gem 'compass-rails'
  gem 'select2-rails'
end

group :deploy do
  gem 'capistrano', require: false
  gem 'capistrano-ext', require: false
  gem 'capistrano_colors', require: false
  gem 'rvm-capistrano', require: false
end

group :development do
  gem 'pry'
  gem 'mechanize'
  gem "letter_opener"
  gem 'rails-dev-tweaks', '~> 0.6.1'
end

group :test do
  gem 'rspec-rails', '2.7.0', :require => false
  gem 'mongoid-rspec'
  gem 'remarkable_mongoid', :require => 'remarkable/mongoid'
end

group :staging, :production do
  gem 'therubyracer' # JavaScript compiler
end