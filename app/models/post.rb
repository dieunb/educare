class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Slug
  include ScopedSearch::Model

  paginates_per 3

  field :title, type: String
  field :description, type: String
  field :body, type: String

  field :photo_uid, type: String
  image_accessor :photo

  attr_accessible :title, :description, :body, :user_id, :category_id, :photo, :photo_uid

  belongs_to :user
  belongs_to :category

  slug :title

  validates_presence_of :title, presence: true, :message => "This is a required field"
  validates_presence_of :body, presence: true, :message => "This is a required field"

  def thumb_photo_url
    photo ? photo.thumb('226x162').url : Settings.image.default
  end
end