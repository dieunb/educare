class Course
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Slug
  include ScopedSearch::Model

  paginates_per 3

  field :title, type: String
  field :description, type: String
  field :body, type: String
  field :photo_uid, type: String
  image_accessor :photo

  slug :title
  attr_accessible :title, :description, :users, :photo, :photo_uid

  has_and_belongs_to_many :users, class_name: 'User', inverse_of: :courses
  belongs_to :teacher

  def thumb_photo_url
    photo ? photo.thumb('226x162').url : Settings.image.default
  end
end