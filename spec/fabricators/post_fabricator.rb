Fabricator(:post) do
  title { Faker::Lorem.sentence }
  description { Faker::Lorem.sentence(50) }
  body { Faker::Lorem.paragraph(50) }
end