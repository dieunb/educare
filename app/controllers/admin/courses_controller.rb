class Admin::CoursesController < Admin::BaseController

  def index
    @search = Course.scoped_search(params[:search])
    if params[:commit] == 'Search' && !params[:from].blank? && !params[:to].blank?
      @courses = Course.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'), :created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    elsif params[:commit] == 'Search' && !params[:from].blank? && params[:to].blank?
      @courses = Course.where(:created_at.gte => Date.strptime(params[:from], '%d/%m/%Y'))
      params[:to] = Date.today.strftime('%d/%m/%Y')
    elsif params[:commit] == 'Search' && params[:from].blank? && !params[:to].blank?
      @courses = Course.where(:created_at.lte => Date.strptime(params[:to], '%d/%m/%Y'))
    else
      @courses = @search.all
      if params['sort_by_title']
        session['user_sort'] = 'sort_by_title'
        @courses = @courses.sort { |o1, o2| o1.title <=> o2.title }
      else
        session['user_sort'] = nil
      end
    end

    if params['per_page']
      per_page = params['per_page']
      session['per_page'] = params['per_page']
    else
      per_page = 20
      session['per_page'] = nil
    end

    if session['user_sort']
      @courses = Kaminari.paginate_array(@courses).page(params[:page]).per(per_page.to_i)
    else
      @courses = @courses.page(params[:page]).per(per_page.to_i)
    end
  end

  def resource
    @course ||= end_of_association_chain.find_by_slug(params[:id])
  end
end
